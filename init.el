;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Theme
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (tango-dark))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


(global-set-key [f11] 'my-fullscreen)
(defun my-fullscreen ()
(interactive)
(x-send-client-message nil 0 nil "_NET_WM_STATE" 32 '(2 "_NET_WM_STATE_FULLSCREEN" 0))
)

(global-set-key [f9] 'compile)
;; (global-set-key "\C-x\C-b" 'buffer-menu)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(autoload 'ibuffer "ibuffer" "List buffers." t)


(global-linum-mode t)
(tool-bar-mode -1)
(setq column-number-mode t)
(setq confirm-kill-emacs 'y-or-n-p)

;;display time
(display-time-mode 1) ;; 
(setq display-time-24hr-format t) ;;
(setq display-time-day-and-date t) ;;

;; load path
(add-to-list 'load-path' "~/.emacs.d/local")


;; C++ and octave mode and python
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.hpp\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cpp\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cc\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cxx\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))

;; Configure package
(require 'package) ;; You might already have this line
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) ;; You might already have this line


;; C++ style
(require 'google-c-style)
(add-hook 'c-mode-common-hook 'google-set-c-style)
(add-hook 'c-mode-common-hook 'google-make-newline-indent)
(put 'downcase-region 'disabled nil)

;; js
(setq js-indent-level 2)

;;elpy
;; (package-initialize)
(elpy-enable)
;; (setenv "PYTHONPATH" "/usr/local/bin/python3.7")

;; Add cmake listfile names to the mode list.
(setq auto-mode-alist
	  (append
	   '(("CMakeLists\\.txt\\'" . cmake-mode))
	   '(("\\.cmake\\'" . cmake-mode))
	   auto-mode-alist))
(autoload 'cmake-mode "~/.emacs.d/local/cmake-mode.el" t)

;;reopen buffers from last session on startup
(desktop-save-mode 1)

;; Markdown mode
(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

(autoload 'gfm-mode "gfm-mode"
   "Major mode for editing GitHub Flavored Markdown files" t)
(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))



;; Highlight parentheses
(require 'highlight-parentheses)
(define-globalized-minor-mode global-highlight-parentheses-mode
  highlight-parentheses-mode
  (lambda ()
    (highlight-parentheses-mode t)))
(global-highlight-parentheses-mode t)


;; Code hiding
(add-hook 'c-mode-common-hook   'hs-minor-mode)
(add-hook 'emacs-lisp-mode-hook 'hs-minor-mode)
(add-hook 'java-mode-hook       'hs-minor-mode)
(add-hook 'ess-mode-hook        'hs-minor-mode)
(add-hook 'perl-mode-hook       'hs-minor-mode)
(add-hook 'sh-mode-hook         'hs-minor-mode)
(global-set-key [f1] 'hs-toggle-hiding)


;; Rainbow delimiters
(add-to-list 'load-path "~/.emacs.d/elpa/rainbow-delimiters-2.1.3")
(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)


;; Maxima
(add-to-list 'load-path "/usr/share/maxima/5.32.1/emacs/")
(autoload 'maxima-mode "maxima" "Maxima mode" t)
(autoload 'imaxima "imaxima" "Frontend for maxima with Image support" t)
(autoload 'maxima "maxima" "Maxima interaction" t)
(autoload 'imath-mode "imath" "Imath mode for math formula input" t)
(setq imaxima-use-maxima-mode-flag t)
(add-to-list 'auto-mode-alist '("\\.ma[cx]" . maxima-mode))


;; Flycheck
;; (require 'flycheck)
(add-hook 'after-init-hook #'global-flycheck-mode)


;; Swith-window
;; (require 'switch-window)
;; (global-set-key (kbd "C-x o") 'switch-window)


;; Eletric pair
(electric-pair-mode 1)


;; 
(setq compilation-scroll-output t)


;; avoid jump when scrolling
(setq scroll-margin 3
      scroll-conservatively 10000)

;; automatic expand
(global-set-key [(meta ?/)] 'hippie-expand)
(setq hippie-expand-try-functions-list
      '(try-expand-dabbrev
	try-expand-dabbrev-visible
	try-expand-dabbrev-all-buffers
	try-expand-dabbrev-from-kill
	try-complete-file-name-partially
	try-complete-file-name
	try-expand-all-abbrevs
	try-expand-list
	try-expand-line
	try-complete-lisp-symbol-partially
	try-complete-lisp-symbol))

;; mark and jump
(global-set-key [(control ?\.)] 'ska-point-to-register)
(global-set-key [(control ?\,)] 'ska-jump-to-register)
(defun ska-point-to-register()
  "Store cursorposition _fast_ in a register.
hvhvUse ska-jump-to-register to jump back to the stored
position."
  (interactive)
  (setq zmacs-region-stays t)
  (point-to-register 8))

(defun ska-jump-to-register()
  "Switches between current cursorposition and position
that was stored with ska-point-to-register."
  (interactive)
  (setq zmacs-region-stays t)
  (let ((tmp (point-marker)))
        (jump-to-register 8)
        (set-register 8 tmp)))




;; flyspell
(add-hook 'latex-mode-hook 'flyspell-mode)
(put 'set-goal-column 'disabled nil)


;;neo-tree
(add-to-list 'load-path "~/.emacs.d/elpa/neotree-0.5.2") 
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))

(setq dired-dwim-target t)

;;all-the-icons
(require 'all-the-icons)

(require 'helm-config)
(helm-mode 1)
(when (package-installed-p 'helm)
  ;; change default prefix key
  (global-set-key (kbd "C-c h") 'helm-command-prefix)
  (global-unset-key (kbd "C-x c"))

  ;; helm-M-x
  (setq helm-M-x-fuzzy-match t)
  (global-set-key (kbd "M-x") 'helm-M-x)

  ;; helm-kill-ring
  (global-set-key (kbd "M-y") 'helm-show-kill-ring)

  ;; helm-mini
  (global-set-key (kbd "C-x b") 'helm-mini)
  (setq helm-buffers-fuzzy-matching t
        helm-recentf-fuzzy-match t)

  ;; helm-find-files
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  )


;; yasnippet
(add-to-list 'load-path "~/.emacs.d/elpa/yasnippet-0.12.2")
(require 'yasnippet)
(setq yas-snippet-dirs
      '("~/.emacs.d/snippets"                 ;; personal snippets
        ))
(yas-global-mode 1)
(yas/minor-mode-on)
;; auto complete
(add-to-list 'load-path "~/.emacs.d/3rd/fuzzy-el")
(add-to-list 'load-path "~/.emacs.d/3rd/popup-el")
(add-to-list 'load-path "~/.emacs.d/3rd/auto-complete")

(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/3rd/auto-complete/dict")
(ac-config-default)
(defadvice ac-common-setup (after give-yasnippet-highest-priority activate)
  (setq ac-sources (delq 'ac-source-yasnippet ac-sources))
  (add-to-list 'ac-sources 'ac-source-yasnippet))

(require 'dockerfile-mode)
(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))

;;window-numbering 
(window-numbering-mode t)


;;latex
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
   (mapc (lambda (mode)
         (add-hook 'LaTeX-mode-hook mode))
         (list 'auto-fill-mode
               'LaTeX-math-mode
               'turn-on-reftex
               'linum-mode))
(add-hook 'LaTeX-mode-hook
	  (lambda ()
	    (setq TeX-auto-untabify t     ; remove all tabs before saving
		  TeX-engine 'xetex       ; use xelatex default
		  TeX-show-compilation t) ; display compilation windows
	    (TeX-global-PDF-mode t)       ; PDF mode enable, not plain
	    (setq TeX-save-query nil)
	    (imenu-add-menubar-index)
	    (define-key LaTeX-mode-map (kbd "TAB") 'TeX-complete-symbol)))


;;org-mode
(global-set-key (kbd "C-x C-l") 'org-store-link)
(global-set-key (kbd "C-x C-a") 'org-agenda)
(global-set-key (kbd "C-x C-c") 'org-capture)
(global-set-key (kbd "C-x C-s") 'org-switchb)
(add-hook 'org-mode-hook '(lambda () (setq fill-column 80)))
(add-hook 'org-mode-hook 'turn-on-auto-fill)


;;reftex
;; (require-package 'reftex)

;; (require 'reftex)

 

;; (add-hook 'LaTeX-mode-hook 'turn-on-reftex)

;; (setq reftex-plug-into-AUCTeX t)

;; (setq reftex-enable-partial-scans t)

;; (setq reftex-save-parse-info t)

;; (setq reftex-use-multiple-selection-buffers t)

;; (autoload 'reftex-mode "reftex" "RefTeXMinorMode" t)

;; (autoload 'turn-on-reftex "reftex" "RefTeXMinorMode" nil)

;; (autoload 'reftex-citation "reftex-cite" "Makecitation" nil)

;; (autoload 'reftex-index-phrase-mode "reftex-index" "Phrasemode" t)

 

