;;; Compiled snippets and support files for `lisp-interaction-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'lisp-interaction-mode
		     '(("defun" "(defun ${1:fun} (${2:args})\n       $0\n)" "defun" nil nil nil "/home/zju/.emacs.d/snippets/lisp-interaction-mode/defun" nil nil)))


;;; Do not edit! File generated at Sat May 12 10:36:23 2018
