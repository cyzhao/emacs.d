;;; Compiled snippets and support files for `git-commit-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'git-commit-mode
		     '(("ref" "references #${1:100}" "references" nil nil nil "/home/zju/.emacs.d/snippets/git-commit-mode/references" nil nil)
		       ("fix" "fixes #${1:100}" "fixes" nil nil nil "/home/zju/.emacs.d/snippets/git-commit-mode/fixes" nil nil)))


;;; Do not edit! File generated at Sat May 12 10:36:23 2018
