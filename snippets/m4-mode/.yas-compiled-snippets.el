;;; Compiled snippets and support files for `m4-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'm4-mode
		     '(("def" "define(\\`${1:macro}',\\`${2:subst}').\n$0" "def" nil nil nil "/home/zju/.emacs.d/snippets/m4-mode/def" nil nil)))


;;; Do not edit! File generated at Sat May 12 10:36:23 2018
